
//package dsacoursework2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.util.*;

/**
 * @author ajb
 */
public class DictionaryFinder {

    public DictionaryFinder() {
    }

    /**
     * Reads all the words in a comma separated text document into an Array
     *
     * @param f
     */
    public static ArrayList<String> readWordsFromCSV(String file) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(file));
        sc.useDelimiter(" |,");
        ArrayList<String> words = new ArrayList<>();
        String str;
        while (sc.hasNext()) {
            str = sc.next();
            str = str.trim(); //remove whitespace
            str = str.toLowerCase();
            words.add(str);
        }
        return words;
    }

    public static void saveCollectionToFile(Collection<String> c, String file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (String w : c) {
            printWriter.println(w.toString());
        }
        printWriter.close();
    }

    public static TreeMap<String,Integer> formDictionary(Collection<String> c) {
        TreeMap<String, Integer> d = new TreeMap<String, Integer>();

        for (String str: c) {
                if (d.containsKey(str)) {
                    d.put(str, d.get(str) + 1);
                } else {
                    d.put(str, 1);
                }
            }
            return d;
        }


    public static void saveToFile(TreeMap<String,Integer> d, String file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        //Get keys and Values
        for(Map.Entry<String,Integer> entry : d.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            printWriter.println(key + ", " + value);
        }

        printWriter.close();
    }

    public static void main(String[] args) throws Exception {
        DictionaryFinder df = new DictionaryFinder();
        ArrayList<String> in = readWordsFromCSV("TextFiles/lotr.csv");
        //DO STUFF TO df HERE in countFrequencies
        saveCollectionToFile(in,"newFile.txt");
        TreeMap<String,Integer> d = formDictionary(in);
        saveToFile(d, "TextFiles/result.csv");

    }

    /*
    Resources
    https://stackoverflow.com/questions/1318980/how-to-iterate-over-a-treemap
     */

}
