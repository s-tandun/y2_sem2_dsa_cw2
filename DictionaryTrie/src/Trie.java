import java.util.*;

public class Trie {
    TrieNode root;

    public Trie(TrieNode root) {
        this.root = root;
    }

    public Trie() {
        root = new TrieNode();
    }

    public TrieNode getRoot() {
        return root;
    }

    public void setRoot(TrieNode root) {
        this.root = root;
    }

    public boolean add(String key) {
        TrieNode t = root;
        int index;
        for
        (int i = 0; i < key.length(); i++) {
            index = key.charAt(i) - 'a';
            if (t.offspring[index] == null) {
                TrieNode tn = new TrieNode();
                t.offspring[index] = tn;
                t = tn;
                t.setLetter(key.charAt(i));
            } else {
                t = t.offspring[index];
            }
        }
        if (!t.isKey) {
            t.isKey = true;
            return true;
        }
        return false;
    }

    public boolean contains(String key) {
        TrieNode t = root;
        for (int i = 0; i < key.length(); i++) {
            int index = key.charAt(i) - 'a';
            if (t.offspring[index] != null) {
                t = t.offspring[index];
            } else {
                return false;
            }
        }
        return t.isKey;
    }

    public TrieNode getSubTrie(String s) {
        TrieNode t = root;

        for (int i = 0; i < s.length(); i++) {
            int index = s.charAt(i) - 'a';
            if (t.offspring[index] != null) {
                t = t.offspring[index];
            } else {
                return null;
            }
        }

        if (t == root) {
            return null;
        }

        return t;
    }

    public List getAllWords() {
        List list = new ArrayList();
        StringBuilder letters = new StringBuilder();
        Stack<TrieNode> stack = new Stack<>();
        Stack<Tuple> prefixStack = new Stack<>();
        TrieNode curr;
        stack.add(root);

        int numOfOffspring = 0;

        while (!stack.isEmpty()) {
            curr = stack.pop();

           // System.out.println(curr.getLetter() + list.toString() + prefixStack.toString());

            if (!prefixStack.empty()) {

                if (prefixStack.peek().getI() == 0) {
                    prefixStack.pop();
                }

            }

            if (numOfOffspring > 1) {
                if (!prefixStack.empty()) {
                    prefixStack.peek().subtract(1);
                    String prefix = prefixStack.peek().getPrefix();
                    prefix = prefix + letters;
                    prefixStack.add(new Tuple(prefix, numOfOffspring));
                } else {
                    prefixStack.add(new Tuple(letters.toString(), numOfOffspring));
                }

                //System.out.println(prefixStack.toString());
                letters = new StringBuilder();
            }

            if (curr != root) {
                letters.append(curr.getLetter());
            }

            if (curr.isKey && curr != root) {
                String words;
                if (!prefixStack.empty()) {
                    words = prefixStack.peek().getPrefix() + letters;

                } else {
                    words = letters.toString();
                }
                //System.out.println(prefix.toString() + " " + letters);
                list.add(words);
            }

            numOfOffspring = 0;
            for (int i = TrieNode.MAX_OFFSPRING - 1; i >= 0; i--) {
                if (curr.offspring[i] != null) {
                    stack.add(curr.offspring[i]);
                    numOfOffspring++;
                }
            }

            if (curr.isKey && curr != root) {
                if (numOfOffspring == 0) {
                    letters = new StringBuilder();
                    prefixStack.peek().subtract(1);
                }
                if (prefixStack.peek().getI() == 0) {
                    prefixStack.pop();
                }
            }

        }
        return list;
    }

    public String outputBreadthFirstSearch() {
        String output = "";
        Queue<TrieNode> queue = new LinkedList<>();
        queue.add(root);
        TrieNode curr;
        while (!queue.isEmpty()) {
            curr = queue.remove();
            for (int i = 0; i < TrieNode.MAX_OFFSPRING; i++) {
                if (curr.offspring[i] != null) {
                    output += Character.toString((char) (i + 'a'));
                    queue.add(curr.offspring[i]);
                }
            }
        }
        return output;
    }

    public String outputDepthFirstSearch() { //DFS
        String output = "";
        Stack<TrieNode> path = new Stack<>();
        path.push(root);
        TrieNode curr;
        while (!path.isEmpty()) {
            curr = path.pop();


            if (curr.getLetter() != 0) {
                output += Character.toString(curr.getLetter());
            }

            for (int i = TrieNode.getMaxOffspring() - 1; i >= 0; i--) {
                if (curr.offspring[i] != null) {
                    curr.offspring[i].setLetter((char) (i + 'a'));

                    path.add(curr.offspring[i]);
                }
            }
        }
        return output;
    }

    public static void main(String[] args) {
        Trie trie = new Trie();

        System.out.println(trie.add("arose"));
        System.out.println(trie.add("around"));
        System.out.println(trie.add("aroused"));
        System.out.println(trie.add("as"));
        System.out.println(trie.add("ashes"));
        System.out.println(trie.add("aside"));


/*        System.out.println(trie.getRoot());
        //System.out.println(trie.add("cat"));
        //System.out.println(trie.getRoot());
        //System.out.println(trie.contains("hello"));
        System.out.println(trie.outputDepthFirstSearch());
        System.out.println(trie.outputBreadthFirstSearch());
        System.out.println(trie.getSubTrie("ch"));
        System.out.println(trie.contains("cat"));*/
        // System.out.println(trie.outputBreadthFirstSearch());

        System.out.println(trie.getAllWords().toString());
    }
}

//resources
//https://www.programcreek.com/2014/05/leetcode-implement-trie-prefix-tree-java/
//https://leetcode.com/problems/implement-trie-prefix-tree/discuss/58901/trie-using-array
//https://www.geeksforgeeks.org/trie-insert-and-search/
