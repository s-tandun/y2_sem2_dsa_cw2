public class Tuple {

    String prefix;
    int i;

    public Tuple(String prefix, int i) {
        this.prefix = prefix;
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public String getPrefix() {
        return prefix;
    }

    public void subtract(int s) {
        this.i = i-s;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Override
    public String toString() {
        return prefix + '\'' +
                ", " + i ;
    }
}
